CC = clang

CFLAGS=-std=c11 -W -Wall -Wextra -D_GNU_SOURCE -Iinclude/
LDFLAGS=-shared -fPIC

.PHONY: all

all: build build/test build/libmalloc.so

run: build/test build/libmalloc.so
	@LD_PRELOAD=build/libmalloc.so ./build/test

build:
	@mkdir build

build/test: build/main.o
	$(CC) $^ -o $@

build/libmalloc.so: build/malloc.o
	$(CC) $(LDFLAGS) $^ -o $@ -ldl

build/malloc.o: src/malloc.c
	$(CC) $(CFLAGS) -fPIC -nostdlib -c $^ -o $@

build/%.o: src/%.c
	$(CC) $(CFLAGS) -c $^ -o $@

clean:
	@rm -rf build
