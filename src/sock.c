#include "sock.h"

int sock_create(void) {
	int sock_fd = -1;

	if( (sock_fd = socket(AF_UNIX, SOCK_STREAM | SOCK_NONBLOCK, 0)) == -1 ) {
		err(1, "Error while opening a UNIX socket.");
		exit(-1);
	}

	struct sockaddr_un addr = {.sun_family = AF_UNIX, .sun_path = "/tmp/libmalloc.so.socket"};

	if( bind(sock_fd, &addr, sizeof(struct sockaddr_un)) == -1 ) {
		err(1, "Error while connecting to an UNIX socket.");
		exit(-1);
	}

	return sock_fd;
}
