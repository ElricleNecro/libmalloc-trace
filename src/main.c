#include <stdio.h>
#include <stdlib.h>

int main(void) {
	printf("main -> %p.\n", main);
	int *value = (int*)malloc(sizeof(int));
	printf("malloc(%zu) -> %p\n", sizeof(int), value);
	*value = 5;

	printf("Inside main: %d -> %p.\n", *value, value);

	free(value);

	return 0;
}
