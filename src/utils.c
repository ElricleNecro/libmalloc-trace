#include "utils.h"

lmi lmi_append(lmi data, long caller, long stack, long allocated, size_t size) {
	if( data == NULL ) {
		data = (lmi)malloc(sizeof(struct _list_malloc_info_t));
		data->prev = NULL;

		return data;
	} else {
		data->next = (lmi)malloc(sizeof(struct _list_malloc_info_t));
		data->next->prev = data;
		data->next->next = NULL;
		data = data->next;
	}

	data->data.caller_addr = caller;
	data->data.stack_addr = stack;
	data->data.allocated_addr = allocated;
	data->data.size = size;

	return data;
}

void lmi_free(lmi data) {
	while( data != NULL ) {
		lmi tmp = data;
		data = data->prev;
		free(tmp);
	}
}
