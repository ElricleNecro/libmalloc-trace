#include "malloc.h"

static __thread void* (*__malloc)(size_t);
static __thread void* (*___Znwm)(size_t);
static __thread void* (*___Znwj)(size_t);
static __thread void  (*__free)(void*);
static __thread void* (*__calloc)(size_t, size_t);
static __thread void* (*__realloc)(void*, size_t);
static __thread void* (*__reallocarray)(void*, size_t, size_t);

static __thread bool no_hook = false;

void* malloc(size_t size) {
	// If we are running with no hook, just call malloc:
	if( no_hook ) {
		// In case we haven't allocated yet the callback:
		if( __malloc == NULL ) {
			__malloc = dlsym(RTLD_NEXT, "malloc");
		}

		// Calling the callback:
		return __malloc(size);
	}

	// In this part, we do not want to use the hook, just call malloc directly:
	no_hook = true;

	// In case we haven't allocated yet the callback:
	if( __malloc == NULL )
		__malloc = dlsym(RTLD_NEXT, "malloc");

	fprintf(stderr, "MALLOC:: Preparing to allocate %zu bytes.\n", size);

	// Calling the callback:
	void *ptr = (*__malloc)(size);

	fprintf(stderr, "MALLOC:: Allocation %zu at 0x%p for %p, stack pointer: %p.\n", size, ptr, __builtin_return_address(0), __builtin_frame_address(0));

	// Now, we can have hook again:
	no_hook = false;

	return ptr;
}

void* _Znwj(size_t size) {
	// If we are running with no hook, just call _Znwj:
	if( no_hook ) {
		// In case we haven't allocated yet the callback:
		if( ___Znwj == NULL ) {
			___Znwj = dlsym(RTLD_NEXT, "_Znwj");
		}

		// Calling the callback:
		return ___Znwj(size);
	}

	// In this part, we do not want to use the hook, just call _Znwj directly:
	no_hook = true;

	// In case we haven't allocated yet the callback:
	if( ___Znwj == NULL )
		___Znwj = dlsym(RTLD_NEXT, "_Znwj");

	fprintf(stderr, "_Znwj:: Preparing to allocate %zu bytes.\n", size);

	// Calling the callback:
	void *ptr = (*___Znwj)(size);

	fprintf(stderr, "_Znwj:: Allocation %zu at 0x%p for %p, stack pointer: %p.\n", size, ptr, __builtin_return_address(0), __builtin_frame_address(0));

	// Now, we can have hook again:
	no_hook = false;

	return ptr;
}

void* _Znwm(size_t size) {
	// If we are running with no hook, just call _Znwm:
	if( no_hook ) {
		// In case we haven't allocated yet the callback:
		if( ___Znwm == NULL ) {
			___Znwm = dlsym(RTLD_NEXT, "_Znwm");
		}

		// Calling the callback:
		return ___Znwm(size);
	}

	// In this part, we do not want to use the hook, just call _Znwm directly:
	no_hook = true;

	// In case we haven't allocated yet the callback:
	if( ___Znwm == NULL )
		___Znwm = dlsym(RTLD_NEXT, "_Znwm");

	fprintf(stderr, "_Znwm:: Preparing to allocate %zu bytes.\n", size);

	// Calling the callback:
	void *ptr = (*___Znwm)(size);

	fprintf(stderr, "_Znwm:: Allocation %zu at 0x%p for %p, stack pointer: %p.\n", size, ptr, __builtin_return_address(0), __builtin_frame_address(0));

	// Now, we can have hook again:
	no_hook = false;

	return ptr;
}

void free(void *ptr) {
	if( __free == NULL )
		__free = dlsym(RTLD_NEXT, "free");

	fprintf(stderr, "Freeing adress 0x%p.\n", ptr);

	(*__free)(ptr);
}

void* calloc(size_t nmemb, size_t size) {
	if( no_hook ) {
		if( __calloc == NULL ) {
			__calloc = dlsym(RTLD_NEXT, "calloc");
		}
		return __calloc(nmemb, size);
	}

	no_hook = true;

	if( __calloc == NULL )
		__calloc = dlsym(RTLD_NEXT, "calloc");

	void *ptr = __calloc(nmemb, size);

	no_hook = false;

	return ptr;
}

void* realloc(void *ptr, size_t size) {
	if( no_hook ) {
		if( __realloc == NULL ) {
			__realloc = dlsym(RTLD_NEXT, "realloc");
		}
		return __realloc(ptr, size);
	}

	no_hook = true;

	if( __realloc == NULL )
		__realloc = dlsym(RTLD_NEXT, "realloc");

	void *out = __realloc(ptr, size);

	no_hook = false;

	return out;
}

void* reallocarray(void *ptr, size_t nmemb, size_t size) {
	if( no_hook ) {
		if( __reallocarray == NULL ) {
			__reallocarray = dlsym(RTLD_NEXT, "reallocarray");
		}
		return __reallocarray(ptr, nmemb, size);
	}

	no_hook = true;

	if( __reallocarray == NULL )
		__reallocarray = dlsym(RTLD_NEXT, "reallocarray");

	void *out = __reallocarray(ptr, nmemb, size);

	no_hook = false;

	return out;
}
