#ifndef SOCK_H_9GUVKAMT
#define SOCK_H_9GUVKAMT

#include <err.h>
#include <stdlib.h>

#include <sys/socket.h>
#include <sys/un.h>

int sock_create(void);

#endif /* end of include guard: SOCK_H_9GUVKAMT */
