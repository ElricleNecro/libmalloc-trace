#ifndef HANDLER_H_9MKKS2JV
#define HANDLER_H_9MKKS2JV

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <dlfcn.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

void* malloc(size_t size);
void* _Znwm(size_t size);
void* _Znwj(size_t size);
void  free(void *ptr);
void* calloc(size_t nmemb, size_t size);
void* realloc(void *ptr, size_t size);
void* reallocarray(void *ptr, size_t nmemb, size_t size);

#endif /* end of include guard: HANDLER_H_9MKKS2JV */
