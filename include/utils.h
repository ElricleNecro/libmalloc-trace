#ifndef UTILS_H_MINKSQ2Z
#define UTILS_H_MINKSQ2Z

#include <stdlib.h>

#include "data.h"

typedef struct _list_malloc_info_t {
	struct malloc_info_t data;
	struct _list_malloc_info_t *next;
	struct _list_malloc_info_t *prev;
} *lmi;

lmi lmi_append(lmi data, long caller, long stack, long allocated, size_t size);
void lmi_free(lmi data);

#endif /* end of include guard: UTILS_H_MINKSQ2Z */
