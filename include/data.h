#ifndef DATA_H_B0Z8ZO6A
#define DATA_H_B0Z8ZO6A

#include <pthread.h>

struct malloc_info_t {
	pthread_t thread_id;

	long caller_addr;
	long stack_addr;
	long allocated_addr;

	size_t size;
};

#endif /* end of include guard: DATA_H_B0Z8ZO6A */
